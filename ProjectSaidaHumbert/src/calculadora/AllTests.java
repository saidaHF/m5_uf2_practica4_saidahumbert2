package calculadora;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ CalculadoraTestDivisioSaida.class, CalculadoraTestMultiplicacioSaida.class,
		CalculadoraTestRestaSaida.class, CalculadoraTestSumaSaida.class })
public class AllTests {
	
}
