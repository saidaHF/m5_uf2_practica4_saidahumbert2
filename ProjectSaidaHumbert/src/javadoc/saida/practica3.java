package javadoc.saida;

import java.util.Scanner;

/**
 * <h2>clase Calculadora, sirve para realizar operaciones matem�ticas sencillas</h2>
 * 
 * @version 1.01
 * @author Saida Humbert
 * @since 23/02/2022
 */

public class practica3 {
	/**
  	 * Generamos la classe scanner para poder introducir los valores por pantalla
  	 */
    static Scanner reader = new Scanner(System.in);
    /**
  	 * Main de la calculadora, 
  	 * @param args, el main recoge una matriz de tipo String. Esta matriz recoge los valores que introduzcas en el c�digo 
  	 */
	public static void main(String[] args) {
		
		try {
			practica3.divideix(15, 0);
			
		} catch(ArithmeticException e) {
			System.out.println("Excepci� capturada: " + e);
		}
		
		
		int op1 = 0, op2 = 0;
		char opcio;
		int resultat = 0;
		boolean control = false;

		do {
			mostrar_menu();
			opcio = llegirCar();

			switch (opcio) {
                case 'o':
                case 'O':
                    op1 = llegirEnter();
                    op2 = llegirEnter();
                    control = true;
                    break;
                case '+':
                    if (control)
                        resultat = suma(op1, op2);
                    else mostrarError();
                    break;
                case '-':
                    if (control)
                        resultat = resta(op1, op2);
                     else mostrarError();
                    break;
                case '*':
                    if (control)
                        resultat = multiplicacio(op1, op2);
                     else mostrarError();
                    break;
                case '/':
                    if (control) {
                        resultat = divisio(op1, op2);
                        if (resultat == -99)
                            System.out.print("No ha fet la divisi�");
                        else
                             visualitzar(resultat);
					}
		            else mostrarError();
                    break;
                case 'v':
                case 'V':
                    if (control)
                        visualitzar(resultat);
                     else mostrarError();
                    break;
                case 's':
                case 'S':
                    System.out.print("Acabem.....");

                    break;
                default:
                    System.out.print("opci� erronia");
			}
			;
		} while (opcio != 's' && opcio != 'S');
		System.out.print("\nAdeu!!!");

	}

	// PUBLIC METHODS:
	/**
	 * Metodo que muestra si hay un error al introducir los valores
	 * **/
    public static void mostrarError() { 
		System.out.println("\nError, cal introduir primer els valors a operar");
	}
	
  	/**
  	 * Funci�n que realiza la suma de dos numeros
  	 *@param a , primer numero introducido
	 *@param b , segundo numero introducido
	 *@return res , retorna el resultado de la suma	 
  	 */
	public static int suma(int a, int b) {
		int res;
		res = a + b;
		return res;
	}
	
	/**
	 *Funcion que realiza la resta de dos numeros
	 *@param a , primer numero introducido
	 *@param b , segundo numero introducido
	 *@return res , retorna el resultado de la suma 	 
	*/
	public static int resta(int a, int b) { 
		int res;
		res = a - b;
		return res;
	}

	/**
	* Funcion que realiza la multiplicaci�n de dos numeros
	 *@param a , primer numero introducido
	 *@param b , segundo numero introducido
	 *@return res , retorna el resultado de la suma 
	*/
	public static int multiplicacio(int a, int b) { 
		int res;
		res = a * b;
		return res;
	}

	/**
	 * Funcion que divide dos numeros
	 * @return <ul>
	 *            <li>M : muestra el residuo si seleccionamos M</li>
	 *            <li>D : muestra el resultado de la division si seleccionamos D</li>
	 *         </ul>
	 *@param a , primer numero introducido
	 *@param b , segundo numero introducido
	 */
	public static int divisio(int a, int b) { 
		int res = -99;
		char op;

		do {
			System.out.println("M. " + a + " mod " + b);
			System.out.println("D  " + a + " / " + b);
			op = llegirCar();
			if (op == 'M' || op == 'm'){
                if (b == 0) 
                    System.out.print("No es pot dividir entre 0\n");
                else
                    res = a % b;
			}

			else if (op == 'D' || op == 'd'){
                    if (b == 0)
                        System.out.print("No es pot dividir entre 0\n");
                    else
                        res = a / b;
                    }
                else
                    System.out.print("opci� incorrecte\n");
		} while (op != 'M' && op != 'm' && op != 'D' && op != 'd');

		return res;
	}

	/**
	 *Funcion para leer los parametros pasados por pantalla
	 * @return <ul>
	 *             <li>Primero nos pide por pantalla que introduzcas un caracter</li>
	 *             <li>Despues de introducir un valor valido se guarda en una varaible</li>
	 *        </ul>
	 */
	public static char llegirCar()
	{
		char car;

		System.out.print("Introdueix un car�cter: ");
		car = reader.next().charAt(0);

		//reader.nextLine();
		return car;
	}
	/**
	 *Funcion para dividir dos numeros
	 * @return <ul>
	 *           <li>Divide entre 0</li>       
	 *        </ul>
	 *@param num1 , primer numero introducido
	 *@param num2 , segundo numero introducido
	 */
	public static  int divideix(int num1, int num2) {
		int res;
	if (num2 == 0)
		throw new java.lang.ArithmeticException("Divisi� entre zero");
	else 
	res = num1/num2;
		return res;
	}

	/**
	 *  Funcion para leer los numeros introducidos por pantalla
	 * @return <ul>
	 *              <li>Primero pide por pantalla que introduzcas un numero</li>
	 *              <li> Una vez introducido por teclado se guarda en una varaible</li>
	 *              <li>En el caso de que no sea un numero el valor introducido nos mostrara un ERROR</li>
	 *         </ul>
	 */
	public static int llegirEnter() 
	{
		int valor = 0;
		boolean valid = false;

		do {
			try {
				System.out.print("Introdueix un valor enter: ");
				valor = reader.nextInt();
				valid = true;
			} catch (Exception e) {
				System.out.print("Error, s'espera un valor enter");
				reader.nextLine();
			}
		} while (!valid);

		return valor;
	}
	/**
	 *  Funcion mostrar el resultado
	 * @param res, el resultado de la operacion
	 */
	public static void visualitzar(int res) { 
		System.out.println("\nEl resultat de l'operacio �s " + res);
	}
	/**
	 *  Funci�n d�nde tenemos las opciones del menu
	 *  <ul>
	 *             <li>"o" ,Obtenemos los valores</li>
	 *             <li>"+" ,Hace una suma de los valores</li>
	 *             <li>"-" ,Hace una resta de los valores</li>
	 *             <li>"*" ,Hace una multiplicacion de los valores</li>
	 *             <li>"/" ,Hace una division de los valores</li>
	 *             <li>"v" ,Visualizar el resultado</li>
	 *             <li>"s" ,Salir del programa</li>                 
	 * </ul>
	 */
	public static void mostrar_menu() {
		System.out.println("\nCalculadora:\n");
		System.out.println("o.- Obtenir els valors");
		System.out.println("+.- Sumar");
		System.out.println("-.- Restar");
		System.out.println("*.- Multiplicar");
		System.out.println("/.- Dividir");
		System.out.println("v.- Visualitzar resultat");
		System.out.println("s.- Sortir");
		System.out.print("\n\nTria una opci� i ");
	}

}